<?php

use Doctum\Doctum;
use Symfony\Component\Finder\Finder;
use Doctum\RemoteRepository\GitHubRemoteRepository;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('resources')
    ->exclude('tests')
    ->in('./app');


// change Template title, vendor and name
return new Doctum($iterator, [
    'title'                => 'Template',
    'language'             => 'fr', // Could be 'fr'
    'build_dir'            => __DIR__ . '/docs/build',
    'cache_dir'            => __DIR__ . '/docs/cache',
    'remote_repository'    => new GitHubRemoteRepository('vendor/name', 'https://gitlab.com/vendor/name'),
    'default_opened_level' => 2,
]);
