#!/bin/bash

ROOT_PATH='./vendor/dendev/qa/.scripts'

if ! [ -f ./docs ];
then
    mkdir docs
fi

cp ${ROOT_PATH}/templates/index.html ./docs

${ROOT_PATH}/generate_doc.sh
${ROOT_PATH}/generate_coverage.sh
${ROOT_PATH}/generate_metrics.sh


if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ./docs/index.html;
fi;

exit $?;
