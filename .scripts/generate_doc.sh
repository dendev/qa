#!/bin/bash

ROOT_PATH='./'

if ! [ -f ${ROOT_PATH}doctum.phar ];
then
    curl -O https://doctum.long-term.support/releases/latest/doctum.phar
    cp ${ROOT_PATH}vendor/dendev/qa/.scripts/templates/doctum.php $ROOT_PATH;
fi

php ${ROOT_PATH}doctum.phar update ${ROOT_PATH}doctum.php;

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ${ROOT_PATH}docs/build/index.html;
fi;

exit $?;
