# Qa

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

```bash
composer require dendev/qa
```

## Usage

### Docs/Metrics
Make phpdoc 
```php
php artisan qa:generate_doc
```

Make phpmetrics
```bash
php artisan qa:generate_metrics
```

Make code coverage
```bash
php artisan qa:generate_coverage
```

Make report ( with all previous tools )

```bash
php artisan qa:generate_report
```

#### Open browser
You can use -o or --open to automatically open browser on result
```bash
php artisan qa:generate_report --open 
```

#### Alias 
You can use short version of cmd
```bash
php artisan qa:gr -o 
```

### Utils
Check laravel php dependencies
```bash
php artisan qa:check_dependencies
```

Set correct permissions
```bash
php artisan qa:set_perms
```

## Rmq
Perhaps you need to generate a conform phpunit.xml 
```bash
phpunit --generate-configuration
``` 

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

