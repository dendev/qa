<?php

namespace Dendev\Qa;

use Dendev\Qa\Console\Commands\CheckDependencies;
use Dendev\Qa\Console\Commands\GenerateCoverage;
use Dendev\Qa\Console\Commands\GenerateDoc;
use Dendev\Qa\Console\Commands\GenerateMetrics;
use Dendev\Qa\Console\Commands\GenerateReport;
use Dendev\Qa\Console\Commands\SetPerms;
use Illuminate\Support\ServiceProvider;

class QaServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'dendev');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'dendev');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/qa.php', 'qa');

        // Register the service the package provides.
        $this->app->singleton('qa', function ($app) {
            return new Qa;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['qa'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/qa.php' => config_path('qa.php'),
        ], 'qa.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/dendev'),
        ], 'qa.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/dendev'),
        ], 'qa.assets');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/dendev'),
        ], 'qa.lang');*/

        // Registering package commands.
        $this->commands([
            SetPerms::class,
            CheckDependencies::class,
            GenerateCoverage::class,
            GenerateMetrics::class,
            GenerateDoc::class,
            GenerateReport::class,
        ]);
    }
}
