<?php

namespace Dendev\Qa\Facades;

use Illuminate\Support\Facades\Facade;

class Qa extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'qa';
    }
}
