<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class GenerateReport extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:generate_report {--o|open : open default browser on docs index.html}';
    protected $aliases = [
        'qa:gr'
    ];
    protected $script_label = "Generate report";
    protected $script_name = 'generate_report.sh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate full report with phpdoc, phpmetrics, codecoverage';
}
