<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class GenerateMetrics extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:generate_metrics {--o|open : open default browser on metrics index.html}';
    protected $aliases = [
        'qa:gm'
    ];
    protected $script_label = "Generate metrics";
    protected $script_name = 'generate_metrics.sh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate phpmetrics report';
}
