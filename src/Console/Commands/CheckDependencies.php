<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class CheckDependencies extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:check_dependencies';
    protected $aliases = [
        'qa:cd'
    ];
    protected $script_label = "Check dependencies";
    protected $script_name = 'check_dependencies.sh';
    protected $show_output = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if php dependencies are installed';
}
