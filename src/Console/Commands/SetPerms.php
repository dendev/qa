<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class SetPerms extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:set_perms';
    protected $aliases = [
        'qa:sp'
    ];
    protected $script_label = "Set Perms";
    protected $script_name = 'set_perms.sh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set basic permissions for storage directory and bootstrap/cache';
}
