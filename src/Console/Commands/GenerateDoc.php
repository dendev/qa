<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class GenerateDoc extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:generate_doc {--o|open : open default browser on phpdoc index.html}';
    protected $aliases = [
        'qa:gd'
    ];
    protected $script_label = "Generate documentation";
    protected $script_name = 'generate_doc.sh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate php documentation with doctum';
}
