<?php

namespace Dendev\Qa\Console\Commands;

use Dendev\Qa\Traits\UtilCommand;
use Illuminate\Console\Command;

class GenerateCoverage extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qa:generate_coverage {--o|open : open default browser on coverage dashboard.html}';
    protected $aliases = [
        'qa:gc'
    ];
    protected $script_label = "Generate coverage";
    protected $script_name = 'generate_coverage.sh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate test coverage resume';
}
