<?php

namespace Dendev\Qa\Traits;

trait UtilCommand
{
    protected $script_path = 'vendor/dendev/qa/.scripts/';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $output = null;

        $this->_check_script();
        $result = $this->_exec_script($output);
        $this->_inform($result, $output);
    }

    private function _check_script(): void
    {
        $full_path = $this->_get_script_full_path();

        if (!file_exists($full_path))
            throw new \LogicException("script {$this->script_name} not found in $full_path");

        if (!is_executable($full_path)) {
            $ok = chmod($full_path, 0755);

            if (!$ok)
                throw new \LogicException("script {$this->script_name} is not executable in $full_path");
        }
    }

    private function _get_script_full_path(): string
    {
        return base_path($this->script_path . $this->script_name);

    }

    private function _exec_script(&$output): ?string
    {
        $result = null;

        $full_path = $this->_get_script_full_path();

        $arg = array_key_exists('open', $this->option()) && $this->option('open') !== false ? '-o' : '';

        $result = exec("bash $full_path $arg 2>&1 ; echo $?", $output, $result);

        return $result;
    }

    private function _inform($result, $output): void
    {
        $this->line($this->script_label);

        if( isset($this->show_output) && $this->show_output )
            $this->line($output);

        if (intval($result) === 0)
            $this->info("V Done");
        else
            $this->error("X Failed");
    }
}
